package ru.poas.heat_map.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.poas.heat_map.model.Owner;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@ToString
public class OwnerDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @NotNull
    private String nickname;

    @Getter
    @Setter
    @NotNull
    private String login;

    @Getter
    @Setter
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public OwnerDto(Owner owner) {
        this.id = owner.getId();
        this.nickname = owner.getNickname();
        this.login = owner.getLogin();
    }
}
